package gffFiles;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.biojava3.genome.parsers.gff.FeatureI;
import org.biojava3.genome.parsers.gff.FeatureList;
import org.biojava3.genome.parsers.gff.GFF3Reader;

public class GFFParser 
{
	private File gffFile; 
	
	public void initiate( final File gffFile )
	{
		setGffFile( gffFile );
	}
	
	/**
	 * Retrieve all exons. 
	 * 1. Identify exons
	 * 2. Add to ArrayList 
	 * @return
	 * @throws IOException
	 */
	public ArrayList<FeatureI> getExons() throws IOException
	{
		ArrayList<FeatureI> exons = new ArrayList<FeatureI>(); 
		
		FeatureList features = GFF3Reader.read( getGffFile().getAbsolutePath() );

		//Select all exons identified by the type/feature "exon" 
		for( int i = 0; i < features.size(); i++ )
		{
			if( features.get( i ).type().equals("exon") )
			{
				exons.add( features.get( i ) ); 
			} 
		}
		
		return exons; 
	}
	
	
	/**
	 * Remove those exons with the same starting and end locations
	 * @param exons
	 * @return
	 */
	public ArrayList<FeatureI> removeDuplicateExons( final ArrayList<FeatureI> exons )
	{ 
		HashMap<String, FeatureI> exonMap = new HashMap<String, FeatureI>();
		for( FeatureI exon : exons )
		{ 
			final String currentPos = "" +  exon.location().bioStart() + ";" + exon.location().bioEnd(); 
			//System.out.println( currentPos ); 
			exonMap.put( currentPos, exon );   
		}
		
		ArrayList<FeatureI> out = new ArrayList<FeatureI>(); 
		Collection<FeatureI> uniq = exonMap.values(); 
		for( FeatureI f : uniq )
			out.add( f ); 
		
		
		sortFeatureListByPos( out );
		
		for( FeatureI f : out )
			System.out.println( f ); 
		
		return out; 
	}
	
	public void sortFeatureListByPos( final ArrayList<FeatureI> list )
	{
		Comparator<FeatureI> comp = new Comparator<FeatureI>() 
		{
			public int compare(FeatureI o1, FeatureI o2) {
				
				if( o1.location().bioStart() > o2.location().bioStart() )
					return 1;
				else if( o1.location().bioStart() < o2.location().bioStart() )
					return -1;
				else
				{
					if( o1.location().bioEnd() > o2.location().bioEnd() )
						return 1;
					else if( o1.location().bioEnd() < o2.location().bioEnd() )
						return -1;
					else 
						return 0; 
				}
			}
		};
		
		Collections.sort(list, comp );
	}
	
	//Getter 
	public File getGffFile() { return this.gffFile; }
	
	//Setter
	private void setGffFile( final File gffFile ) { this.gffFile = gffFile; } 
	
	public static void main(String[] args) throws IOException 
	{
		final File gffFile = new File("/Volumes/DiskA/timecourses/zebrafish/Danio_rerio_Ensembl_Zv9/Danio_rerio/Ensembl/Zv9/Annotation/Genes/genesPartNeg.gtf");  
		
		GFFParser gffParser = new GFFParser(); 
		gffParser.initiate( gffFile ); 
		
	}

}
